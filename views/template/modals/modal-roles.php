<div class="modal fade" id="modalFormRol" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Nuevo rol</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="tile">
          <div class="tile-body">
            <form id="formRol" name="formRol">
              <div class="form-group">
                <label class="control-label">Nombre</label>
                <input id="txtNombre" name="txtNombre" type="text" class="form-control" placeholder="Nombre del rol" required>
              </div>
              <div class="form-group">
                <label class="control-label">Descripción</label>
                <textarea class="form-control" name="txtDescripcion" id="txtDescripcion" rows="2" placeholder="Descripción del rol" required></textarea>
              </div>
              <div class="form-group">
                <label class="control-label">Estatus</label>
                <select name="listEstatus" id="listEstatus" class="form-control" required>
                  <option value="1">Activo</option>
                  <option value="2">Inactivo</option>
                </select>
              </div>
              <div class="tile-footer">
                <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Guardar</button>
                <button class="btn btn-secondary"><i class="fa fa-times"></i> Cancelar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>