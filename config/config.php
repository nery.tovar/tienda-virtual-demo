<?php
  const BASE_URL = 'http://localhost/proyectos/tienda-virtual-demo';
  date_default_timezone_set('America/Mexico_City');
  const DB_HOST = 'localhost';
  const DB_NAME = 'tienda_virtual';
  const DB_USER = 'root';
  const DB_PASSWORD = '';
  const DB_CHARSET = 'charset=utf8';
  const SPD = '.';
  const SPM = ',';
  const SMONEY = '$';
?>