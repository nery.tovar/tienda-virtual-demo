var tableRoles;

document.addEventListener('DOMContentLoaded', function() {
  tableRoles = $('#tableRoles').dataTable({
    'aProcessing': true,
    'aServerSide': true,
    'language': {
      'url': '//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json'
    },
    'ajax': {
      'url': ' ' + baseUrl + '/roles/getRoles',
      'dataSrc': ''
    },
    'columns': [
      {'data': 'id_rol'},
      {'data': 'nombre_rol'},
      {'data': 'descripcion_rol'},
      {'data': 'estatus_rol'},
      {'data': 'acciones_rol'}
    ],
    'responsive': 'true',
    'bDestroy': true,
    'iDisplayLength': 10,
    'order': [[0, 'asc']]
  });

  // Nuevo rol
  var formRol = document.querySelector('#formRol');
  formRol.onsubmit = function(e) {
    e.preventDefault();
    var strNombre = document.querySelector('#txtNombre').value;
    var strDescripcion = document.querySelector('#txtDescripcion').value;
    var intStatus = document.querySelector('#listEstatus').value;

    if(strNombre == '' || strDescripcion == '' || intStatus == '') {
      swal('Atención', 'Todos los campos son obligatorios.', 'error');
      return false;
    }

    var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    var ajaxUrl = baseUrl + '/roles/setRol';
    var formData = new FormData(formRol);
    request.open('POST', ajaxUrl, true);
    request.send(formData);
    request.onreadystatechange = function() {
      if(request.readystate == 4 && request.status == 200) {
        console.log(request.responseText);
      }
      
    }
  }
});

$('#tableRoles').DataTable();

function openModal() {
  $('#modalFormRol').modal('show');
}