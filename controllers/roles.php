<?php
    class Roles extends Controllers {
    public function __construct() {
      parent::__construct();
    }

    public function roles() {
      $data['page_id'] = 3;
      $data['page_tag'] = 'Roles de usuario';
      $data['page_name'] = 'rol_usuario';
      $data['page_title'] = 'Roles de usuario - <small>Tienda Virtual</small>';
      
      $this->views->getView($this, 'roles', $data);
    }

    public function getRoles() {
      $arrData = $this->model->selectRoles();

      for($i = 0; $i < count($arrData); $i++) {
        if($arrData[$i]['estatus_rol'] == 1) {
          $arrData[$i]['estatus_rol'] = '<span class="badge badge-success">Activo</span>';
        } else {
          $arrData[$i]['estatus_rol'] = '<span class="badge badge-danger">Inactivo</span>';
        }

        $arrData[$i]['acciones_rol'] = '<div class="text-center">
                                          <button class="btn btn-secondary btn-sm btnPermisosRol" rl="' . $arrData[$i]['id_rol'] . '" title="Permisos"><i class="fa fa-lock"></i></button>
                                          <button class="btn btn-primary btn-sm btnEditRol" rl="' . $arrData[$i]['id_rol'] . '" title="Editar"><i class="fa fa-pencil"></i></button>
                                          <button class="btn btn-danger btn-sm btnDelRol" rl="' . $arrData[$i]['id_rol'] . '" title="Eliminar"><i class="fa fa-trash"></i></button>
                                        </div>';
      }
      echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
      die();
    }

    public function setRol() {
      dep($_POST);
    }
  }
?>