<?php
  class rolesModel extends Mysql {
    public function __construct() {
      parent::__construct();
    }

    public function selectRoles() {
      $sql = "SELECT * FROM tienda_virtual.roles WHERE estatus_rol != 0";
      $request = $this->select_all($sql);

      return $request;
    }
  }
?>